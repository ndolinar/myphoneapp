package com.example.myphoneapp

import android.telecom.Call
import android.telecom.InCallService
import android.widget.Toast

class CallService : InCallService() {
    override fun onCallAdded(call: Call) {
        OngoingCall.call = call;

        // OngoingCall.call = call;
         CallActivity.start(this, call)

    }

    override fun onCallRemoved(call: Call?) {
        OngoingCall.call = null;
    }
}