package com.example.myphoneapp

import android.Manifest
import android.Manifest.permission.CALL_PHONE
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.ToneGenerator
import android.net.Uri
import android.os.Bundle
import android.telecom.TelecomManager
import android.util.Log
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker.PERMISSION_GRANTED
import androidx.core.net.toUri
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.concurrent.schedule

class MainActivity : AppCompatActivity() {
    // yttut
    private lateinit var mEditTextNumber: EditText;
    companion object {
        val REQUEST_CODE = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        mEditTextNumber = findViewById(R.id.edit_text_number)
        val imageCall: ImageView = findViewById(R.id.image_call)
        imageCall.setOnClickListener {
            makePhoneCall()
        }

        replaceDefaultDialer()
    }

    private fun replaceDefaultDialer() {
        // THIS IS NOT WORKING
        if (getSystemService(TelecomManager::class.java).defaultDialerPackage != packageName) {
            Intent(TelecomManager.ACTION_CHANGE_DEFAULT_DIALER)
                .putExtra(TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, packageName)
                .let(::startActivity)
        }

//        val telManager = getSystemService(TELECOM_SERVICE) as TelecomManager
//        if (packageName != telManager.defaultDialerPackage) {
//            val intent = Intent(TelecomManager.ACTION_CHANGE_DEFAULT_DIALER)
//                .putExtra(TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, packageName)
//            startActivity(intent)
//        }
    }

    private fun makePhoneCall() {
        val number = mEditTextNumber.text.toString()
        if (number.trim().length > 0) {
            if (ContextCompat.checkSelfPermission(this, CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission not granted. REQUESTING...", Toast.LENGTH_SHORT).show()
                ActivityCompat.requestPermissions(this, arrayOf(CALL_PHONE), REQUEST_CODE)
            } else {
                val myUri:Uri = Uri.parse("tel:" + number.trim { it <= ' ' })
                val myIntent = Intent(Intent.ACTION_CALL, myUri)
                startActivity(myIntent)

            }
        } else {
            Toast.makeText(this, "Enter phone number", Toast.LENGTH_SHORT).show()
        }

//        Log.v("asd", "Random message")
//        val dtmfGenerator = ToneGenerator(1, ToneGenerator.MAX_VOLUME)
//        dtmfGenerator.startTone(ToneGenerator.TONE_DTMF_0, 2000)
//        Timer("Stop tone", false).schedule(200) {
//            dtmfGenerator.startTone(ToneGenerator.TONE_DTMF_1, 2000)
//            Timer("New tone", false).schedule(200) { dtmfGenerator.stopTone() }
//        }
//        Timer("Stop tone", false).schedule(200) {
//            dtmfGenerator.stopTone()
//            dtmfGenerator.startTone(ToneGenerator.TONE_DTMF_1, 200000)
//            Timer("New tone", false).schedule(400) { dtmfGenerator.stopTone() }
//        }
//        dtmfGenerator.stopTone()
//        dtmfGenerator.startTone(ToneGenerator.TONE_DTMF_8, 250)
//        dtmfGenerator.stopTone()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CODE) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission GRANTED", Toast.LENGTH_SHORT).show()
                // Show dialog to make this app the default app for calling
//                if (getSystemService(TelecomManager::class.java).defaultDialerPackage !== packageName) {
//                    Intent(TelecomManager.ACTION_CHANGE_DEFAULT_DIALER).putExtra(TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, packageName)
//                            .let(::startActivity)
//                }
                makePhoneCall()
            } else {
                Toast.makeText(this, "Permission DENIED", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
