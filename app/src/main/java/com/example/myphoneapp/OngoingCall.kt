package com.example.myphoneapp

import android.telecom.Call
import android.telecom.VideoProfile
import android.util.Log
import android.widget.Toast
import io.reactivex.subjects.BehaviorSubject

object OngoingCall {
    val state: BehaviorSubject<Int> = BehaviorSubject.create()

    private val callback = object : Call.Callback() {
        override fun onStateChanged(call: Call, newState: Int) {
//            Log.v("log tag", "##################################################### call.toString(): " + call.toString())
//            Toast.makeText(this, "OOOOoooo...", Toast.LENGTH_SHORT)
            state.onNext(newState)
        }
    }

    var call: Call? = null
    set(value) {
        field?.unregisterCallback(callback)
        value?.let {
            it.registerCallback(callback)
            state.onNext(it.state)
        }
        field = value
    }

    fun answer() {
        call!!.answer(VideoProfile.STATE_AUDIO_ONLY)
    }

    fun hangup() {
        call!!.disconnect()
    }

    fun playDtmfTone(input: Char) {
        Log.v("log tag", "Play DTMF tone: $input")
        call!!.playDtmfTone(input)
        call!!.stopDtmfTone()
    }

}